# CYK-Parser

Yet another CYK parser.

## Description

This app implements the CYK algorithm for parsing words, generates
the derivation trees and order them based on its probability.

It uses a grammar file defined in simple text format, following
some rules (see example.txt in grammars directory).
You only have to select a grammar, type a word and the app will do the trick: 
the app shout if the word is valid or not and, if it is, generates all
derivation trees.

## Third Party Libraries

* [JavaScript InfoVis Toolkit](http://thejit.org/): A JS library
used to generate the derivations trees.
* [960 Grid System](http://960.gs/): A grid system (you don't say!)...
well, it's a grid system, I think you know what it does.

## Authors

* Adolfo Henrique Schneider
* Lucas Jose Kreutz Alves
* Rodrigo Zanella Ribeiro

## License

The good and old MIT License. See license.txt file.

-------------------------------------------------

Formal Languages and Automata, Computer Science, Institute of Informatics   
Federal University of Rio Grande do Sul (UFRGS), Brazil

Linguagens Formais e Automatos, Ciencia da Computacao,
Instituto de Informatica  
Universidade Federal do Rio Grande do Sul (UFRGS)
