<?php

require_once("cyk-parser.php");

try
{
	$sent = false;
	
	if(isset($_POST['sent'])) {
		$sent = true;
		
		$grammarFile = isset($_POST['grammar-file']) ? $_POST['grammar-file'] : '';
		$word =  isset($_POST['word']) ? $_POST['word'] : '';
		
		$parser = new CYKParser("grammars/" . $grammarFile);
		$parser->setWord($word);
		$result = $parser->parse();
	} else {
		$word = '';
		$grammarFile = '';
	}
}
catch(Exception $e)
{
	$error = $e->getMessage();
}

// open thie grammars directory
$dir = opendir("grammars");

// get grammar files (actually, it shows any file that is not unix hiden :)
while($file = readdir($dir)) {
	if(!preg_match("/^\..*/", $file))
		$grammarFiles[] = $file;
}
?>

<?php include("public/header.php"); ?>

	<form action="index.php" method="post">
		<input type="hidden" value="sent" name="sent" />
		<div class="grid_24">
		<fieldset>
			<dl>
				<dt>
					<label>Selecione a Gramática:</label>
				</dt>
				<dd>
					<select name="grammar-file">
						<?php foreach($grammarFiles as $file): ?>
							<option value="<?php echo $file?>" <?php if($file == $grammarFile):?>selected="selected"<?php endif;?>>
								<?php echo $file?></option>
						<?php endforeach; ?>
					</select>
					(<a href="upload.php">enviar nova gramática</a>)
				</dd>
			</dl>
			<dl>
				<dt>
					<label>Palavra:</label>
				</dt>
				<dl>
					<input type="text" name="word" value="<?php echo $word?>" /> separe cada símbolo por espaço. Ex: ( x + x )
				</dl>
			</dl>
			
			<dl>
				<dd><button type="submit">Go, go, go!</button></dd>
			</dl>
		</fieldset>
		</div>
	</form>

<?php if($sent && !isset($error)): ?>
	<h1>Resultado:</h1>
	<?php if($result): ?>
	<p class="accepted">Palavra <span class="word"><?php echo $word; ?></span> foi ACEITA na gramática <?php echo $grammarFile ?></p>
	<?php else: ?>
	<p class="rejected">Palavra <span class="word"><?php echo $word; ?></span> foi REJEITADA na gramática <?php echo $grammarFile ?></p>
	<?php endif; ?>
	
	<h1>Matriz CYK:</h1>
	<table id="cyk-matrix">
		<?php 
		$cykMatrix = $parser->getCYKMatrix();
		$greaterRow = isset($cykMatrix[0]) ? count($cykMatrix[0]) : 0;
		for($i = count($cykMatrix) - 1; $i >= 0; $i--): ?>
		<tr>
			<?php for($j = 0; $j < $greaterRow; $j++): ?>
			<td class="<?php echo isset($cykMatrix[$i][$j]) ? "not-empty-cell": '' ?>">
				<?php echo isset($cykMatrix[$i][$j]) ? implode(", ", $cykMatrix[$i][$j]) : '&nbsp;' ?>
			</td>
			<?php endfor; ?>
		</tr>
		<?php endfor; ?>
		<tr>
			<?php 
			$terminals = explode(' ', $word);
			for($i=0; $i<count($terminals);$i++): ?>
				<td class="terminal-cell"><?php echo $terminals[$i]; ?></td>
			<?php endfor; ?>
		</tr>
	</table>
	
	<h1>Árvores de Derivação:</h1>
	<div id="trees-container">
		<?php
		$numberOfTrees = count($parser->getPossibleTrees());
		$trees = $parser->getPossibleTrees();
		?>
		
		<?php
		for($i=1; $i<=$numberOfTrees;$i++):
		?>
			<?php if($i != 1 && ($i-1) % 3 == 0): ?>
			<div class="clear"></div>
			<?php endif; ?>	
			<div class="grid_8">
				<h4>Árvore <?php echo $i; ?> - Prob: <?php echo $parser->calculateTreeProbability($trees[$i-1]);?></h4>
				<div id="tree_<?php echo $i?>" class="tree-container""></div>
			</div>
		<?php endfor; ?>
	</div>
	<div class="clear"></div>

	<h1>Produções:</h1>
	<pre><?php print_r($parser->getProductions()); ?></pre>
	
	<h1>Probabilidades:</h1>
	<pre><?php print_r($parser->getProbabilities()); ?></pre>
	
	<h1>Variáveis:</h1>
	<pre><?php print_r($parser->getVariables()); ?></pre>
	
	<h1>Terminais:</h1>
	<pre><?php print_r($parser->getTerminals()); ?></pre>
	
	<h1>Símbolo Inicial:</h1>
	<pre><?php print_r($parser->getInitialSymbol()); ?></pre>
	

<?php elseif($sent && isset($error)): ?>
	<div class="grid_24"><div class="error-message"><?php echo $error; ?></div></div>	
<?php endif; ?>


<script language="JavaScript" type="text/javascript">
	var labelType, useGradients, nativeTextSupport, animate;

(function() {
  var ua = navigator.userAgent,
      iStuff = ua.match(/iPhone/i) || ua.match(/iPad/i),
      typeOfCanvas = typeof HTMLCanvasElement,
      nativeCanvasSupport = (typeOfCanvas == 'object' || typeOfCanvas == 'function'),
      textSupport = nativeCanvasSupport 
        && (typeof document.createElement('canvas').getContext('2d').fillText == 'function');
  //I'm setting this based on the fact that ExCanvas provides text support for IE
  //and that as of today iPhone/iPad current text support is lame
  labelType = (!nativeCanvasSupport || (textSupport && !iStuff))? 'Native' : 'HTML';
  nativeTextSupport = labelType == 'Native';
  useGradients = nativeCanvasSupport;
  animate = !(iStuff || !nativeCanvasSupport);
})();

var Log = {
  elem: false,
  write: function(text){
    if (!this.elem) 
      this.elem = document.getElementById('log');
    this.elem.innerHTML = text;
    this.elem.style.left = (500 - this.elem.offsetWidth / 2) + 'px';
  }
};


<?php for($i=0; $i<count($parser->getPossibleTrees());$i++):?>
function init<?php echo $i?>(){
    //init data
    <?php echo $parser->getTrees(); ?>
    //end
    
    //A client-side tree generator
    var getTree = (function() {
        var i = 0;
        return function(nodeId, level) {
          var subtree = eval('(' + trees[0].replace(/id:\"([a-zA-Z0-9]+)\"/g, 
          function(all, match) {
            return "id:\"" + match + "_" + i + "\""  
          }) + ')');
          $jit.json.prune(subtree, level); i++;
          return {
              'id': nodeId,
              'children': subtree.children
          };
        };
    })();
    
    //Implement a node rendering function called 'nodeline' that plots a straight line
    //when contracting or expanding a subtree.
    $jit.ST.Plot.NodeTypes.implement({
        'nodeline': {
          'render': function(node, canvas, animating) {
                if(animating === 'expand' || animating === 'contract') {
                  var pos = node.pos.getc(true), nconfig = this.node, data = node.data;
                  var width  = nconfig.width, height = nconfig.height;
                  var algnPos = this.getAlignedPos(pos, width, height);
                  var ctx = canvas.getCtx(), ort = this.config.orientation;
                  ctx.beginPath();
                  if(ort == 'left' || ort == 'right') {
                      ctx.moveTo(algnPos.x, algnPos.y + height / 2);
                      ctx.lineTo(algnPos.x + width, algnPos.y + height / 2);
                  } else {
                      ctx.moveTo(algnPos.x + width / 2, algnPos.y);
                      ctx.lineTo(algnPos.x + width / 2, algnPos.y + height);
                  }
                  ctx.stroke();
              } 
          }
        }
          
    });

    //init Spacetree
    //Create a new ST instance
    var st = new $jit.ST({
        'injectInto': 'tree_<?php echo $i+1?>',
        //set duration for the animation
        duration: 100,
        //set animation transition type
        transition: $jit.Trans.Quart.easeInOut,
        //set distance between node and its children
        levelDistance: 25,
        
        Navigation: {  
		  enable: true,  
		  type: 'auto',  
		  panning: true, //true, 'avoid nodes'  
		  zooming: false  
		},
		
		offsetY: 170,
		
        //set max levels to show. Useful when used with
        //the request method for requesting trees of specific depth
        levelsToShow: 1000,
        orientation: 'top',
        
        Node: {
            height: 38,
            width: 20,
            //use a custom
            //node rendering function
            type: 'nodeline',
            color:'#23A4FF',
            lineWidth: 2,
            align:"left",
            overridable: true
        },
        
        Edge: {
            type: 'bezier',
            lineWidth: 2,
            color:'#23A4FF',
            overridable: true
        },
        
        //This method is called on DOM label creation.
        //Use this method to add event handlers and styles to
        //your node.
        onCreateLabel: function(label, node){
            label.id = node.id;            
            label.innerHTML = node.name;
            label.onclick = function(){
                st.onClick(node.id);
            };
            //set label styles
            var style = label.style;
            style.width = 20 + 'px';
            style.height = 38 + 'px';            
            style.color = '#000';
            style.cursor = 'pointer';
            //style.backgroundColor = '#1a1a1a';
            style.fontSize = '1em';
            style.textAlign= 'center';
            style.textDecoration = 'none';
            style.paddingTop = '3px';
        }        
    });
    //load json data
    st.loadJSON(eval( '(' + trees[<?php echo $i?>] + ')' ));
    //compute node positions and layout
    st.compute();
    //emulate a click on the root node.
    st.onClick(st.root);
    //end
}
init<?php echo $i?>();
<?php endfor;?>
</script>

<?php include("public/footer.php"); ?>