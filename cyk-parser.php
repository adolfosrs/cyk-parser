<?php

/**
 * Copyright (c) 2012 Adolfo Schneider, Lucas Kreutz, Rodrigo Zanella
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

require_once("node.php");

/**
 * CYK-Parser
 *
 * Yet another CYK parser.
 *
 * Linguagens Formais e Automatos
 * Ciencia da Computacao, UFRGS, 2012/1
 *
 * Authors:
 *   Adolfo Schneider
 *   Lucas Jose Kreutz Alves
 *   Rodrigo Zanella
 */

class CYKParser {
	protected $word = "";
	protected $terminals = array();
	protected $variables = array();
	protected $productions = array();
	protected $probabilities = array();
	protected $initialSymbol = '';
	protected $cykMatrix = array();
	protected $cykHistoricMatrix = array();
	protected $cykTreesMatrix = array();
	protected $gambiarra = 0;

	/**
	 * Constructs the CYKParser object
	 */
	public function __construct($filename) {
		$this -> load($filename);
	}

	/**
	 * Loads the grammar from the file
	 */
	public function load($filename) {
		if (!is_file($filename)) {
			throw new Exception("File doesn't exist.");
		}

		$currentBlock = "";
		$line = 0;
		$handler = fopen($filename, 'r');

		if ($handler) {
			while (!feof($handler)) {
				$line = $line + 1;

				// Read one line of the grammar file
				$buffer = fgets($handler);

				// Removes the comments
				$buffer = preg_replace("/^([^#]*)(#.*)$/", '${1}', $buffer);
				// Removes the trailing white spaces
				$buffer = preg_replace("/[\s]+$/", "", $buffer);
				// Removes the leading white spaces
				$buffer = preg_replace("/^[\s]+/", "", $buffer);

				if ($buffer == "Terminais") {
					$currentBlock = strtolower($buffer);
				} elseif ($buffer == "Variaveis") {
					$currentBlock = strtolower($buffer);
				} elseif ($buffer == "Inicial") {
					$currentBlock = strtolower($buffer);
				} elseif ($buffer == "Regras") {
					$currentBlock = strtolower($buffer);
				} else {
					if ($currentBlock == "terminais") {
						// Clean the terminal
						$buffer = preg_replace("/^(\[ )([^\[\]#]+)( \])$/", "$2", $buffer);
						$this -> addTerminal($buffer);
					} elseif ($currentBlock == "variaveis") {
						// Clean the variable
						$buffer = preg_replace("/^(\[ )([^\[\]#]+)( \])$/", "$2", $buffer);
						$this -> addVariable($buffer);
					} elseif ($currentBlock == "inicial") {
						// Clean the symbol
						$buffer = preg_replace("/^(\[ )([^\[\]#]+)( \])$/", "$2", $buffer);
						$this -> setInitialSymbol($buffer);
					} elseif ($currentBlock == "regras") {
						// Get the variable
						if (preg_match("/^(\[ )([^\[\]#]+)( \])/", $buffer, $variable)) {
							$variable = $variable[2];

							// Removes the left side of production rule
							$buffer = preg_replace("/^(\[ [^\[\]#]+ \] > )(.*)/", "$2", $buffer);

							// Get the right side items (productions)
							$production = "";
							while (preg_match("/^(\[ )([^\[\]#]+)( \])/", $buffer, $tmpProduction)) {
								// Adds the production separator
								// (S->AB prodc. => $this.productions['S'] = ('A#B')
								if (!empty($production)) {
									$production = $production . '#';
								}

								$production = $production . $tmpProduction[2];

								// Removes the production that was added
								$buffer = preg_replace("/^\[ ([^\[\]#]+) \]\s{0,1}(.*)/", "$2", $buffer);
							}
							
							// Get the probabilities
							$probability = 1;
							if(preg_match("/;([0-9\.]+)/", $buffer, $matches)) {
								$probability = floatval($matches[1]);
							}
							
							$this -> addProduction($variable, $production, $probability);
						}
					} elseif (strlen($buffer) > 0) {
						// Not empty line and it's not inside any grammar block: ERROR!
						throw new Exception("Error found on grammar structure ({$filename}, line: {$line})");
					}
				}
			}

			fclose($handler);
		} else {
			throw new Exception("Couldn't open grammar file ({$filename}).");
		}
	}

	/**
	 * Adds a terminal to the terminals array
	 */
	public function addTerminal($terminal) {
		array_push($this -> terminals, $terminal);
	}

	/**
	 * Adds a variable to the variables array
	 */
	public function addVariable($variable) {
		array_push($this -> variables, $variable);
	}

	/**
	 * Sets the initial symbol
	 */
	public function setInitialSymbol($symbol) {
		$this -> initialSymbol = $symbol;
	}

	/**
	 * Add a production to the productions array
	 */
	public function addProduction($left, $right, $probability) {
		// Checks if the left symbol already has a production defined
		if (array_key_exists($left, $this -> productions)) {
			// Don't add if it's already there
			if (!in_array($right, $this -> productions[$left])) {
				array_push($this -> productions[$left], $right);
			}
		} else {
			$this -> productions[$left] = array($right);
		}
		
		// Add the probability
		$derivationKey = array_search($right, $this -> productions[$left]);
		$this->probabilities[$left][$derivationKey] = $probability;
	}

	/**
	 * Return the array of productions
	 */
	public function getProductions() {
		return $this -> productions;
	}
	
	/**
	 * Return the array of probabilities
	 */
	public function getProbabilities() {
		return $this -> probabilities;
	}
	
	/**
	 * Returns the probability of a given production
	 */
	public function getProbability($left, $right) {
		if(! isset($this -> productions[$left])) {
			throw new Exception("Production doesn't exist.");
		}
		
		$derivationKey = array_search($right, $this -> productions[$left]);
		
		if($derivationKey === FALSE) {
			throw new Exception("Production rule doesn't exist.");
		}
		
		return $this->probabilities[$left][$derivationKey];
	}

	/**
	 * Return the array of variables
	 */
	public function getVariables() {
		return $this -> variables;
	}

	/**
	 * Return the array of terminals
	 */
	public function getTerminals() {
		return $this -> terminals;
		$this -> setWord($_POST['word']);
		echo $this -> word;
	}

	/**
	 * Return the initial symbol
	 */
	public function getInitialSymbol() {
		return $this -> initialSymbol;
	}

	/**
	 * Return the word
	 */
	public function getWord() {
		return $this -> word;
	}

	/**
	 * Returns the CYK Matrix
	 */
	public function getCYKMatrix() {
		return $this -> cykMatrix;
	}

	/**
	 * Returns the CYK Historic Matrix
	 */
	public function getCYKHistoricMatrix() {
		return $this -> cykHistoricMatrix;
	}
	
	/**
	 * Returns the CYK Trees Matrix
	 */
	public function getCYKTreesMatrix() {
		return $this -> cykTreesMatrix;
	}

	/**
	 * Set the word to be parsed
	 */
	public function setWord($word) {
		if (strlen($word) > 0) {
			$this -> word = $word;
		} else {
			throw new Exception("The word is invalid. Must have 1 character at least.");
		}
	}
	

	/*
	 * CYK Algorithm - Make the CYK's matrix
	 */
	public function parse() {
		$cont_arr = 0;
		$this -> cykMatrix = array();
		$result = true;

		//divide word
		$word = explode(" ", $this -> word);

		//count number of words
		$wordSize = count($word) - 1;

		//verifies if all word's items are terminals
		for ($i = 0; $i <= $wordSize; $i++) {
			$result = $result && in_array($word[$i], $this -> terminals);
		}

		//if all the terminals in the word are a terminal in the grammar, make the cyk table
		if ($result === true) {
			// generates the first row (the most low line in the table)
			$this -> cykMatrix[0] = array();

			//first step of algorithm - Variables that generate the terminals
			for ($i = 0; $i <= $wordSize; $i++) {
				$this -> cykMatrix[0][$i] = $this -> generatorsOfSymbol($this -> productions, $word[$i]);

				for ($j = 0; $j < count($this -> cykMatrix[0][$i]); $j++) {
					$this -> cykHistoricMatrix[0][$i][$j] = $this -> cykMatrix[0][$i][$j] . '[' . $word[$i] . ']';
					
					// Creates the basic nodes
					$this -> cykTreesMatrix[0][$i][$j] = new Node($this -> cykMatrix[0][$i][$j]);
					$probability = $this->getProbability($this -> cykMatrix[0][$i][$j], $word[$i]);
					$this -> cykTreesMatrix[0][$i][$j]->setProbability($probability);
					$this -> cykTreesMatrix[0][$i][$j]->setLeftChild(new Node($word[$i]));
				}
			}

			//second step of algorithm - rule of sheave
			for ($line = 1; $line <= $wordSize; $line++) {

				//creates the diagonal items of the pulley
				for ($n = 0; $n <= ($wordSize - $line); $n++) {
					$this -> cykMatrix[$line][$n] = array();

					//creates the vertical items of the pulley
					for ($c = 1; $c <= $line; $c++) {
						$vector = array();
						$vector = $this -> mx_to_vt($this -> cykMatrix[$c - 1][$n], $this -> cykMatrix[$line - $c][$n + $c]);

						//mount the matrix line
						for ($i = 0; $i <= (count($vector) - 1); $i++) {
							$this -> cykMatrix[$line][$n] = array_unique(array_merge($this -> cykMatrix[$line][$n], $this -> generatorsOfSymbol($this -> productions, $vector[$i])));
						}
					}
				}
			}

			
			//second step of algorithm - rule of sheave
			for ($line = 1; $line <= $wordSize; $line++) {

				//creates the diagonal items of the pulley
				for ($n = 0; $n <= ($wordSize - $line); $n++) {

					//updates the historic matrix
					$this -> cykHistoricMatrix[$line][$n] = array();
					$this -> cykTreesMatrix[$line][$n] = array();
					$branch = 0;
					
					//creates the vertical items of the pulley
					for ($c = 1; $c <= $line; $c++) {
						
						//get elements of the current cyk matrix cell
						for ($j = 0; $j < count($this -> cykMatrix[$line][$n]); $j++) {
							$parent = $this -> cykMatrix[$line][$n][$j];
							
							for ($k = 0; $k < count($this -> cykHistoricMatrix[$c - 1][$n]); $k++) {
								$leftChild = $this -> cykHistoricMatrix[$c - 1][$n][$k];
								$treeLeftChild = $this -> cykTreesMatrix[$c - 1][$n][$k];
								
								for ($l = 0; $l < count($this -> cykHistoricMatrix[$line - $c][$n + $c]); $l++) {
									$rightChild = $this -> cykHistoricMatrix[$line - $c][$n + $c][$l];
									$treeRightChild = $this -> cykTreesMatrix[$line - $c][$n + $c][$l];
									
									//checks if the production is valid
									if(in_array($this->getParentFromHistoricString($leftChild).'#'.$this->getParentFromHistoricString($rightChild), $this->productions[$parent])) {
										//the # separates the left and right child
										$node = $parent . '[' . $leftChild . '#' . $rightChild . ']';
										
										$prob = $this->getProbability($parent, $this->getParentFromHistoricString($leftChild).'#'.$this->getParentFromHistoricString($rightChild));
										
										$tree = new Node($parent);
										$tree->setProbability($prob);
										$tree->setLeftChild($treeLeftChild);
										$tree->setRightChild($treeRightChild);

										// avoid repetitions of nodes
										if(!in_array($node, $this -> cykHistoricMatrix[$line][$n])) {
											$this -> cykHistoricMatrix[$line][$n][$branch] = $node;
											$this -> cykTreesMatrix[$line][$n][$branch] = $tree;
											$branch++;
										}
									}
								}
							}
						}
					}
				}
				
				
			}


			// Verifies if the initial symbol is in the root
			$result = in_array($this -> initialSymbol, $this -> cykMatrix[$wordSize][0]);
		}

		return $result;
	}

	/**
	 * Calculates the probabilty of tree
	 */
	public function calculateTreeProbability($tree) {
		if($tree == null ||$tree->getProbability() == null) {
			return 1.0;
		}
		
		return $tree->getProbability() * $this->calculateTreeProbability($tree->getLeftChild()) * $this->calculateTreeProbability($tree->getRightChild());
	}

	/*
	 * concatenates elements to a vector
	 */
	public function mx_to_vt($matrix1, $matrix2) {
		$vector = array();
		//1º case: both are arrays
		if (is_array($matrix1)) {
			if (is_array($matrix2)) {
				$tamanho1 = count($matrix1) - 1;
				$tamanho2 = count($matrix2) - 1;
				for ($i = 0; $i <= $tamanho1; $i++) {
					$conc1 = $matrix1[$i];
					for ($j = 0; $j <= $tamanho2; $j++) {
						$conc2 = $matrix2[$j];
						$conc = $conc1 . '#' . $conc2;
						array_push($vector, $conc);
					}
				}
			}
			//2º case: only matrix1 is a array
			else {
				$tamanho1 = count($matrix1) - 1;
				$conc2 = $matrix2;
				for ($i = 0; $i <= $tamanho1; $i++) {
					$conc1 = $matrix1[$i];
					$conc = $conc1 . '#' . $conc2;
					array_push($vector, $conc);
				}
			}
		} else {
			//3º case: only matrix2 is a array
			if (is_array($matrix2)) {
				$tamanho2 = count($matrix2) - 1;
				$conc1 = $matrix1;
				for ($i = 0; $i <= $tamanho2; $i++) {
					$conc2 = $matrix2[$i];
					$conc = $conc1 . '#' . $conc2;
					array_push($vector, $conc);
				}
			}
			//neither is a array
			else {//nenhum dos dois é vetor
				$conc1 = $matrix1;
				$conc2 = $matrix2;
				$conc = $conc1 . '#' . $conc2;
				array_push($vector, $conc);
			}
		}
		array_unique($vector);
		return $vector;
	}

	/*
	 * Selects variables which generates a symbol
	 */
	public function generatorsOfSymbol($productions, $symbol) {
		$set = array();
		$cont_arr = 0;
		foreach ($productions as $index => $value) {// sweeps the array of productions
			$tamanho = count($value) - 1;
			for ($i = 0; $i <= $tamanho; $i++) {
				if (strcmp($symbol, $value[$i]) == 0) {//compare if $symbol belongs to the productions
					if (!(in_array($value[$i], $set))) {
						$set[$cont_arr] = $index;
						$cont_arr++;
						break;
					} else {
						break;
					}
				}
			}
		}
		return array_unique($set);
	}
	
	/**
	 * Given a node, returns the parent
	 * NODE: VAR[SON#SON] or VAR[terminal], where SON is a child node (left and right)
	 */
	public function getParentFromHistoricString($node) {
		return preg_match("/^([^\[\]#]+)/", $node, $matches) > 0 ? $matches[1] : null;
	}
	
	/**
	 * Returns an array of the possible derivation trees
	 */
	public function getPossibleTrees() {
		$possibleTrees = array();
		
		$topCellIndex = count($this->cykTreesMatrix) - 1;
		
		if(isset($this->cykTreesMatrix[$topCellIndex][0])) {
			$topCykTreesCell = $this->cykTreesMatrix[$topCellIndex][0];
			
			for($i = 0; $i < count($topCykTreesCell); $i++) {
				// get only the trees that the root is the initial symbol
				if($topCykTreesCell[$i]->getContent() === $this->initialSymbol) {
					array_push($possibleTrees, $topCykTreesCell[$i]);
				}
			}
			
			$swap = true;
			while($swap) {
				$swap = false;
				
				for($i=0; $i <= count($possibleTrees)-2; $i++) {
					$prob1 = $this->calculateTreeProbability($possibleTrees[$i]);
					$prob2 = $this->calculateTreeProbability($possibleTrees[$i+1]);
					
					// swap
					if($prob1 < $prob2) {
						$tmp = $possibleTrees[$i];
						$possibleTrees[$i] = $possibleTrees[$i+1];
						$possibleTrees[$i+1] = $tmp;
						
						$swap = true;
					}
				}
			}
		}
		
		return $possibleTrees;
	}
	
	/**
	 * Um comentário bem supimpa explicando o que a função faz
	 */
	public function central($tree){
		if($tree !== null) {
			echo $tree->getContent().",";
			$this->central($tree->getLeftChild());
			$this->Central($tree->getRightChild());		
		}
	}
	
	/**
	 * Print all the possible trees
	 */
	public function getTrees(){
		
		$trees = $this->getPossibleTrees();
		
		$jsArrayOfTree = "var trees=new Array();\n";
	
		for($i=0; $i < count($trees); $i++) {
			$jsArrayOfTree .= "trees[{$i}]=\"".$this->getDataTrees($trees[$i])."\";\n";
		}
		
		return $jsArrayOfTree;
	}
	
	/**
	 * Mount the "json" variable to print the javascript tree
	 */
	
	public function getDataTrees($tree){
		$this->gambiarra++;
		$str = "";
		
		if ($tree !== null){
			$name = ($tree->getProbability() == null) ? $tree->getContent() : $tree->getContent().', '.$tree->getProbability();
			$str = "{id:\\\"node".$this->gambiarra++."\\\", name:\\\"".$name."\\\", data:{}, ";
			
			if($tree->getLeftChild() !== null && $tree->getRightChild() !== null) {
				$children = $this->getDataTrees($tree->getLeftChild()).','.$this->getDataTrees($tree->getRightChild());
			} else {
				$children = $this->getDataTrees($tree->getLeftChild()).$this->getDataTrees($tree->getRightChild());
			}
			
			$str .= "children:[".$children."]}";
			
		}
		return $str;
	}
	
	
}
